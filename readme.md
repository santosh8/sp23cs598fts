# CS598 FTS Warmup Assignment

### Requirements
- Python3

### Set-up:

- If the testing enviroment is Cloud-Lab then no modifications are needed to the scripts

- If the testing is not on Cloud-Lab or not using node0 to node4 as servers, then please edit the ```client.py``` file, replace the servernames with your server ip addresses. (replace node0, node1 etc with ip addresses)

To set up the key value store follow the steps:
1. SSH into 5 servers from node0 to node4
2. ```git clone https://gitlab.engr.illinois.edu/santosh8/sp23cs598fts.git```
3. ```cd sp23cs598fts```
4. ```python3 replica.py 8000```

The key value store should be up and ready to serve requests from clients.

To stop replicas, kill the replica processes using ```ctrl+c```

To set up clients:
1. SSH into the client servers
2. ```git clone https://gitlab.engr.illinois.edu/santosh8/sp23cs598fts.git```
3. ```cd sp23cs598fts```

### Usage of clients

To set a key value pair
```
python3 client.py write key value
```
e.g:
```
python3 client.py write a 1
```

To read a key value pair
```
python3 client.py read key
```

e.g:
```
python3 client.py read a
```

### Automation

***To get write only statistics***
```
python3 write_automation.py no_of_key_value_pairs clients
```
no_of_key_value_pairs: No.of key value pairs to write into the store.
clients: To run multiple threads of clients, default value is 1. For running multiple clients give the desired number

e.g:
```
python3 write_automation.py 100 1
```
The above command will populate 100 key value pairs and print the statistics related to the operations


***To get read only statistics***
```
python3 read_automation.py no_of_key_value_pairs mode clients
```
no_of_key_value_pairs: No.of key value pairs to write into the store

mode: r or wr

    - r: If key-value pairs are already populated by running write-automation script above
    - wr: To populate key-value pairs (will overwrite if key-value paris are already present)

clients: To run multiple threads of clients, default value is 1. For running multiple clients give the desired number

e.g:

1.
```
python3 read_automation.py 100 r 1
```

2.
```
python3 read_automation.py 100 r 4
```

3.
```
python3 read_automation.py 100 wr 1
```


***To get Read-Write statistics***
```
python3 read_write_automation.py no_of_key_value_pairs mode clients
```
no_of_key_value_pairs: No.of key value pairs to write into the store

mode: r or wr

    (This only refers to data population, not the automation, the automation will still do write even if mode is r)

    - r: If key-value pairs are already populated by running write-automation script above
    - wr: To populate key-value pairs (will overwrite if key-value paris are already present)
clients: To run multiple threads of clients, default value is 1. For running multiple clients give the desired number. 

e.g:

1.
```
python3 read_write_automation.py 100 r 1
```

2.
```
python3 read_write_automation.py 100 r 2
```

3.
```
python3 read_write_automation.py 100 wr 1
```