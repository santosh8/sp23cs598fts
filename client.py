import socket
import pickle
import random
import sys

# replance node0, node1, node2, node3, node4 with your server addresses if not using cloudlab
server_addresses = [
    ("node0", 8000),
    ("node1", 8000),
    ("node2", 8000),
    ("node3", 8000),
    ("node4", 8000),
]


def get(key):
    random_servers = random.sample(server_addresses, 3)
    responses = []
    for server_address in random_servers:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect(server_address)
                s.sendall(pickle.dumps(("read", key)))
                response = pickle.loads(s.recv(1024))
                responses.append(response)
        except:
            pass
    if len(responses) < 3:
        return None
    max_ts = -1
    latest_pair = None
    for pair in responses:
        if pair[1][1] > max_ts:
            max_ts = pair[1][1]
            latest_pair = pair
    for server_address in server_addresses:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect(server_address)
                s.sendall(
                    pickle.dumps(
                        (
                            "write",
                            key,
                            latest_pair[0],
                            (latest_pair[1][0], latest_pair[1][1]),
                        )
                    )
                )
                response = pickle.loads(s.recv(1024))
        except:
            pass
    return latest_pair


def set(key, v):
    random_servers = random.sample(server_addresses, 3)
    responses = []
    for server_address in random_servers:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect(server_address)
                s.sendall(pickle.dumps(("read", key)))
                response = pickle.loads(s.recv(1024))
                responses.append(response)
        except:
            pass
    if len(responses) < 3:
        return None
    max_ts = -1
    latest_pair = None
    for pair in responses:
        if pair[1][1] > max_ts:
            max_ts = pair[1][1]
            latest_pair = pair[1]
    ack_count = 0
    for server_address in server_addresses:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect(server_address)
                s.sendall(pickle.dumps(("write", key, v, (1, max_ts + 1))))
                response = pickle.loads(s.recv(1024))
                if response:
                    ack_count += 1
        except:
            pass
    if ack_count < 3:
        return False
    return True


def main():
    if sys.argv[1] == "write":
        try:
            key, value = sys.argv[2], sys.argv[3]
            write_ok = set(key, value)
            if write_ok:
                print("Write operation succeeded!")
            else:
                print("Write operation failed.")
        except:
            print("Write operation failed, reason can be because of server is down.")
    elif sys.argv[1] == "read":
        try:
            key = sys.argv[2]
            latest_pair = get(key)
            if latest_pair is None:
                print("Read operation failed.")
            else:
                print("Current value of register: {}, timestamp: {}".format(latest_pair[0], latest_pair[1][1]))
        except:
            print("Read operation failed, reason can be because of wrong key or server is down.")


if __name__ == "__main__":
    main()
