import os
import statistics
import time
import sys
import threading

cap = int(sys.argv[1])
mode = "r"
clients = 1
out = []
if len(sys.argv) > 2:
    mode = sys.argv[2]
if len(sys.argv) > 3:
    clients = int(sys.argv[3])

def write():
    if mode == "wr" or mode == "w":
        for i in range(1, cap + 1):
            os.system("python3 client.py write %s %s" % (str(i).zfill(24), str(i).zfill(10)))

def run():
    global out

    if "r" in mode:
        latencies = []
        reads = 0
        writes = 0
        for i in range(1, cap + 1):
            if i % 2 == 0:
                start = time.time()
                os.system(
                    "python3 client.py write %s %s" % (str(i + 1 + cap).zfill(24), str(i + cap + 1).zfill(10))
                )
                end = time.time()
                latencies.append(end - start)
                writes += 1
            else:
                start = time.time()
                os.system("python3 client.py read %s" % str(i).zfill(24))
                end = time.time()
                latencies.append(end - start)
                reads += 1
        out.append((latencies, reads, writes))


threads = []
write()
for i in range(clients):
    t = threading.Thread(target=run)
    threads.append(t)
    t.start()

for t in threads:
    t.join()

client = 1
for i in out:
    latency = i[0]
    reads = i[1]
    writes = i[2]
    print("Read-Write statistics:")
    print("\tClient: %d" % client)
    print("\t\tTotal reads done: %d" % reads)
    print("\t\tTotal writes done: %d" % writes)
    print("\t\tTotal time taken: %f" % sum(latency))
    print("\t\tAverage latency: %f" % statistics.mean(latency))
    print("\t\tMedian latency: %f" % statistics.median(latency))
    print("\t\tStandard Deviation: %f" % statistics.stdev(latency))
    print("\t\tMax latency: %f" % max(latency))
    print("\t\tMin latency: %f" % min(latency))
    print("\t\t99th percentile latency: %f" % sorted(latency)[int(len(latency) * 0.99)])
    print("\t\t95th percentile latency: %f" % sorted(latency)[int(len(latency) * 0.95)])
    # throughput statistics
    print("\t\tThroughput: %f" % (len(latency) / sum(latency)))
    client += 1
