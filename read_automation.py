import os
import statistics
import time
import sys
import threading

cap = int(sys.argv[1])
mode = "r"
clients = 1
out = []
if len(sys.argv) > 2:
    mode = sys.argv[2]

if len(sys.argv) > 3:
    clients = int(sys.argv[3])


def write():
    if mode == "wr" or mode == "w":
        for i in range(1, cap + 1):
            os.system("python3 client.py write %s %s" % (str(i).zfill(24), str(i).zfill(10)))

        print("Writing Done")

def run():
    global out

    if "r" in mode:
        latencies = []
        for i in range(1, cap + 1):
            start = time.time()
            os.system("python3 client.py read %s" % str(i).zfill(24))
            end = time.time()
            latencies.append(end - start)

        out.append(latencies)


threads = []
write()
for i in range(clients):
    t = threading.Thread(target=run)
    threads.append(t)
    t.start()

for t in threads:
    t.join()

client = 1
for i in out:
    print("Read statistics:")
    print("\tClient: %d" % client)
    print("\t\tTotal reads done: %d" % len(i))
    print("\t\tTotal time taken: %f" % sum(i))
    print("\t\tAverage latency: %f" % statistics.mean(i))
    print("\t\tMedian latency: %f" % statistics.median(i))
    print("\t\tStandard Deviation: %f" % statistics.stdev(i))
    print("\t\tMax latency: %f" % max(i))
    print("\t\tMin latency: %f" % min(i))
    print("\t\t99th percentile latency: %f" % sorted(i)[int(len(i) * 0.99)])
    print("\t\t95th percentile latency: %f" % sorted(i)[int(len(i) * 0.95)])
    # throughput statistics
    print("\t\tThroughput: %f" % (len(i) / sum(i)))
    client += 1
