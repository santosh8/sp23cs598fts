import socket
import pickle
import sys
from collections import defaultdict


replicas = defaultdict(lambda: (None, (-1, -1)))


# This function performs the get phase of a read or write operation
def get(key):
    return replicas[key]


# This function performs the set phase of a write operation
def set(key, v, ts_new):
    # The server stores (v,ts-new) if ts-new is greater than the stored timestamp
    if ts_new > replicas[key][1]:
        replicas[key] = (v, (ts_new[0], int(ts_new[1])))
        return True
    return False


# This function handles client requests
def handle_request(conn):
    request = pickle.loads(conn.recv(1024))
    if request[0] == "read":
        key = request[1]
        response = get(key)
        conn.sendall(pickle.dumps(response))
    elif request[0] == "write":
        response = set(request[1], request[2], request[3])
        conn.sendall(pickle.dumps(response))
    elif request[0] == "get":
        key = request[1]
        response = get(key)
        conn.sendall(pickle.dumps(response))
    elif request[0] == "set":
        response = set(request[1], request[2], request[3])
        conn.sendall(pickle.dumps(response))


# This function listens for incoming client connections
def listen(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind(("0.0.0.0", port))
        s.listen()
        while True:
            conn, addr = s.accept()
            print("Connected by", addr)
            try:
                with conn:
                    handle_request(conn)
            except Exception as e:
                print(e)
                print("Error handling request")


if __name__ == "__main__":
    port = int(sys.argv[1])
    listen(port)
