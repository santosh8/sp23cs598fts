import os
import statistics
import time
import sys
import threading


cap = int(sys.argv[1])
clients = 1
out = []
if len(sys.argv) > 2:
    clients = int(sys.argv[2])


def run():
    global out
    latencies = []
    for i in range(1, cap + 1):
        start = time.time()
        os.system("python3 client.py write %s %s" % (str(i).zfill(24), str(i).zfill(10)))
        end = time.time()
        latencies.append(end - start)
    out.append(latencies)


threads = []
for i in range(clients):
    t = threading.Thread(target=run)
    threads.append(t)
    t.start()

for t in threads:
    t.join()


client=1
for i in out:
    print("Write statistics:")
    print("\tClient: %d" % client)
    print("\t\tTotal writes done: %d" % len(i))
    print("\t\tTotal time taken: %f" % sum(i))
    print("\t\tAverage latency: %f" % statistics.mean(i))
    print("\t\tMedian latency: %f" % statistics.median(i))
    print("\t\tStandard Deviation: %f" % statistics.stdev(i))
    print("\t\tMax latency: %f" % max(i))
    print("\t\tMin latency: %f" % min(i))
    print("\t\t99th percentile latency: %f" % sorted(i)[int(len(i) * 0.99)])
    print("\t\t95th percentile latency: %f" % sorted(i)[int(len(i) * 0.95)])
    # throughput statistics
    print("\t\tThroughput: %f" % (len(i) / sum(i)))
    client += 1
