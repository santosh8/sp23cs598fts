import os
import statistics
import time
import sys
import threading

cap = int(sys.argv[1])
clients = int(sys.argv[2])
out = []


def run():
    global out
    latency = []

    for i in range(1, cap + 1):
        start = time.time()
        os.system("redis-cli -c -p 7000 set %s %s" % (str(i).zfill(24), str(i).zfill(10)))
        end = time.time()
        latency.append(end - start)

    out.append(latency)


threads = []
for i in range(clients):
    t = threading.Thread(target=run)
    threads.append(t)
    t.start()

for t in threads:
    t.join()

count = 1
for i in out:
    print("Client %d" % count)
    print("Total time taken: %f" % sum(i))
    print("Average time taken: %f" % statistics.mean(i))
    print("Median time taken: %f" % statistics.median(i))
    print("Standard Deviation: %f" % statistics.stdev(i))
    print("Max time taken: %f" % max(i))
    print("Min time taken: %f" % min(i))
    print("99th percentile: %f" % sorted(i)[int(len(i) * 0.99)])
    print("95th percentile: %f" % sorted(i)[int(len(i) * 0.95)])
    # throughput statistics
    print("Throughput: %f" % (len(i) / sum(i)))
    count += 1
